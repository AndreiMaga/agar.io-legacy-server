package net;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import gameobjects.Point;
import org.joml.Vector3f;

public class PointSerializer extends Serializer<Point> {
  @Override
  public void write(Kryo kryo, Output output, Point object) {
    output.writeInt(object.pid);
    output.writeFloat(object.pos.x);
    output.writeFloat(object.pos.y);
    output.writeFloat(object.pos.z);
  }

  @Override
  public Point read(Kryo kryo, Input input, Class<Point> type) {
    return new Point(input.readInt(), new Vector3f(input.readFloat(), input.readFloat(), input.readFloat()));
  }
}
