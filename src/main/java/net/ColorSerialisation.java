package net;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import java.awt.*;

public class ColorSerialisation extends Serializer<Color> {


  @Override
  public void write(Kryo kryo, Output output, Color object) {
    output.writeFloat((float) object.getRed() / 255);
    output.writeFloat((float) object.getGreen() / 255);
    output.writeFloat((float) object.getBlue() / 255);
  }

  @Override
  public Color read(Kryo kryo, Input input, Class<Color> type) {
    return new Color(input.readFloat(), input.readFloat(), input.readFloat());
  }
}
