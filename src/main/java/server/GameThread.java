package server;

import gameobjects.Player;
import gameobjects.Point;
import javafx.application.Platform;
import net.NetworkProtocol;
import org.joml.Vector3f;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import static server.ServerStart.MAX_MASS;
import static server.ServerStart.MAX_POINT_MASS;
import static server.ServerStart.gridSize;

public class GameThread implements Runnable {

  volatile public ConcurrentHashMap<Integer, Player> players = new ConcurrentHashMap<Integer, Player>();
  volatile public ConcurrentHashMap<Integer, Point> points = new ConcurrentHashMap<Integer, Point>();
  ServerStart st;
  private boolean running = false;
  private Random rand;
  private Thread thread;
  private int[] leaderboard = {0,0,0,0,0};
  //win at MAX_MASS/2 + 1
  private int currentMass = 0;
  private int points_mass =0;
  public GameThread(ServerStart st) {
    this.st = st;
    rand = new Random();
  }

  //Game Thread
  @Override
  public void run() {
    init();
    //delta loop
    long lastTime = System.nanoTime();
    double amountOfTicks = 60.0;
    double ms = 10000000 / amountOfTicks;
    double delta = 0;
    while (running) {
      long now = System.nanoTime();
      delta += (now - lastTime) / ms;
      lastTime = now;
      while (delta >= 1) {
        update();
        delta--;
      }
    }
    stop();
  }

  private void init() {
    //spawn the first MAX_MASS / 10 points // before players can join
    for (int i = 0; i < MAX_MASS / 10; i++) {
      //spawn more points
      spawnPoints();
    }
  }


  public void spawnPoints() {
    int pid = rand.nextInt(99999);
    Point p = new Point(pid, new Vector3f(rand.nextFloat() * gridSize, rand.nextFloat() * gridSize, 0));
    points.put(pid, p);
    NetworkProtocol.AddObject addObject = new NetworkProtocol.AddObject();
    addObject.point = p;
    st.server.sendToAllTCP(addObject);
  }

  public void update() {

    //check for collision
    collision();
    //check the mass
    points();

    //iterate thru' all the points, if they need to be removed, send the req to the server
    points.forEach((pid,point)->{
      if(point.toRemove) {
        NetworkProtocol.RemoveObject removeObject = new NetworkProtocol.RemoveObject();
        removeObject.point = point;
        points.remove(pid);
        st.server.sendToAllTCP(removeObject);
      }
    });
    //iterate thru' all the points, if they won, stop the game, send the WON by calling the network protocol "WIN"
    players.forEach((pid,player)-> {
      if (player.radius >= MAX_MASS / 2 + 1) {
        //the player won the round
        NetworkProtocol.Winner winner = new NetworkProtocol.Winner();
        winner.pid = pid;
        st.server.sendToAllTCP(winner);
      }
      //create leaderboard
    });
//    //send the leaderboard to all the clients
//    NetworkProtocol.LeaderBoard leaderBoard = new NetworkProtocol.LeaderBoard();
//    leaderBoard.pid = leaderboard;
//    st.server.sendToAllTCP(leaderBoard);


  }

  private void points() {
    currentMass = 0;
    points_mass = 0;
    players.forEach((pid, player) -> {
      currentMass += player.radius;
    });
    currentMass += points.size();
    points_mass += points.size();
//    System.out.println("[SERVER] Current points : "+ currentMass);
    if (currentMass < MAX_MASS && points_mass < MAX_POINT_MASS) {
      //spawn 1/5 the difference
      for (int i = 0; i < (MAX_POINT_MASS -  points_mass) ; i++) {
        //spawn more points
        spawnPoints();
      }
    }
  }

  private void collision() {
    players.forEach((targetApid, targetA) -> {
      players.forEach((targetBpid, targetB) -> {
        if (targetApid != targetBpid) {
          //not the same player, check colision
          double distance = targetA.posOnGrid.distance(targetB.posOnGrid);

          if (distance < targetA.radius/2 + targetB.radius/2 && !targetA.isDead && !targetB.isDead) {
            //Colision happens
            //check colision parameters
            if(targetA.radius > targetB.radius + targetB.radius *1/10){
              // A eats B
                targetA.radius += targetB.radius;
                targetB.isDead = true;
            }else if(targetB.radius > targetA.radius + targetA.radius *1/10){
              // B eats A
              targetB.radius += targetA.radius;
              targetA.isDead = true;
            }
          }
        }
      });
    });
    players.forEach((pidA,targetA)->{
      points.forEach((pidB, targetB) -> {
        //check colision with points
        double distance = targetA.posOnGrid.distance(targetB.pos);

        if(distance < targetA.radius/2 + 12/2 && !targetA.isDead){
          //A eats B
          targetA.radius += 1;
          targetB.toRemove = true;
          //flag the removal
        }
      });
    });

  }

  public void start() {
    running = true;
    thread = new Thread(this, "Game");
    thread.start();
  }

  public void stop() {
    running = false;
    try {
      thread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public ConcurrentHashMap<Integer, Player> getPlayers() {
    return players;
  }


  public ConcurrentHashMap<Integer, Point> getPoints() {
    return points;
  }
}
