package server;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener.ThreadedListener;
import com.esotericsoftware.kryonet.Server;
import gameobjects.Player;
import net.NetworkProtocol;

import java.io.IOException;

//ServerSide
public class ServerStart {
  static int gridSize = 8000;
  static int MAX_MASS = 4000;
  static int MAX_POINT_MASS = 1000;
  Server server;
  GameThread gt;


  public ServerStart() throws IOException {
    server = new Server() {
      protected Connection newConnection() {
        return new ServerStart.PlayerConnection();
      }
    };
    server.start();
    NetworkProtocol.register(server);
    server.bind(8989, 54777);//Tcp Port , Udp Port
    GameThread gt = new GameThread(this);
    gt.start();
    server.addListener(new ThreadedListener(new ServerListener(gt, this)));
  }

  public static void main(String[] args) {
    try {
      new ServerStart();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  static class PlayerConnection extends Connection {
    public Player player;
  }

}
