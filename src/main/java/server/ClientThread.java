package server;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import gameobjects.Player;
import net.NetworkProtocol;
import org.joml.Vector3f;

import java.util.Random;

import static server.ServerStart.gridSize;

public class ClientThread extends Listener {

  private Connection connection;
  private Player player;
  private Random rand;
  private GameThread gameThread;
  private ServerStart serverStart;

  public ClientThread(Connection connection, GameThread gameThread, ServerStart serverStart) {
    this.connection = connection;
    this.gameThread = gameThread;
    this.serverStart = serverStart;
    rand = new Random();
    connection.addListener(this);
  }


  @Override
  public void received(Connection connection, Object object) {
    //add the logic for each thread right here

    ServerStart.PlayerConnection con = (ServerStart.PlayerConnection) connection;
    player = con.player;
    if (object instanceof NetworkProtocol.Disconnect) {
      disconnected(con);
    }
    if (object instanceof NetworkProtocol.Login) {
      //if player is already logged in return
      if (player != null) return;
      NetworkProtocol.Login login = (NetworkProtocol.Login) object;
      //reject if the name is invalid
      String name = login.name;
      if (!isValid(name)) {
        con.close();
        return;
      }
      //reject if already logged in
      gameThread.getPlayers().forEach((k, v) -> {
        if (v.name.equals(name)) {
          con.close();
        }
      });
      player = new Player(name, new Vector3f(rand.nextFloat() * gridSize, rand.nextFloat() * gridSize, 0), rand.nextInt(9999), 25, login.color);
      player.posOnScreen = login.posOnScreen;
      logginIn(con, player);
      return;
    }

    if (object instanceof NetworkProtocol.MoveObject) {
      //need to update the player's position
      if (player == null) return;
      NetworkProtocol.MoveObject msg = (NetworkProtocol.MoveObject) object;
      Vector3f mouse = msg.playerMousePosition;
      Player playerToMove = gameThread.players.get(msg.pid);
      //calculate the new position with
      getAcc(playerToMove, mouse);

      //check if collision
      clamp(playerToMove);
      NetworkProtocol.UpdateObject update = new NetworkProtocol.UpdateObject();
      update.player = playerToMove;
      update.isDead = playerToMove.isDead;
      serverStart.server.sendToAllTCP(update);
    }

  }

  private void logginIn(ServerStart.PlayerConnection con, Player player) {
    con.player = player;

    //add the existing characters in the client's buffer
    gameThread.getPlayers().forEach((k, v) -> {
      NetworkProtocol.AddObject addObject = new NetworkProtocol.AddObject();
      addObject.player = v;
      con.sendTCP(addObject);
    });

    //add the existing points in the client's buffer
    gameThread.getPoints().forEach((k, v) -> {
      NetworkProtocol.AddObject addObject = new NetworkProtocol.AddObject();
      addObject.point = v;
      con.sendTCP(addObject);
    });

    //add the player to the list
    gameThread.getPlayers().put(player.pid, player);

    NetworkProtocol.Login add = new NetworkProtocol.Login();
    add.mapWidth = gridSize;
    add.mapHeight = gridSize;
    add.player = player;

    con.sendTCP(add);

    //add the new logged in character to all the clients
    NetworkProtocol.AddObject addObject = new NetworkProtocol.AddObject();
    addObject.player = player;
    serverStart.server.sendToAllTCP(addObject);

  }

  private void getAcc(Player playerToMove, Vector3f mouse) {
    Vector3f temp = new Vector3f(0);
    if (playerToMove.posOnScreen != new Vector3f(0) && mouse != new Vector3f(0)) {
      temp.x = mouse.x - playerToMove.posOnScreen.x;
      temp.y = mouse.y - playerToMove.posOnScreen.y;
      temp = temp.normalize();
      temp = temp.mul((float) ((3 / Math.sqrt(playerToMove.radius)))); // make this dependent of size
      playerToMove.posOnGrid.x = playerToMove.posOnGrid.x + temp.x;
      playerToMove.posOnGrid.y = playerToMove.posOnGrid.y + temp.y;
    }

  }

  private boolean isValid(String value) {
    if (value == null)
      return false;
    value = value.trim();
    return value.length() != 0;
  }

  private void clamp(Player player) {
    if (player.posOnGrid.x <= 0) {
      player.posOnGrid.x = 0;
    } else if (player.posOnGrid.x >= gridSize) {
      player.posOnGrid.x = gridSize;
    }
    if (player.posOnGrid.y <= 0) {
      player.posOnGrid.y = 0;
    } else if (player.posOnGrid.y >= gridSize) {
      player.posOnGrid.y = gridSize;
    }
  }

}
