package server;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import net.NetworkProtocol;

import java.util.HashMap;


public class ServerListener extends Listener {
  GameThread gameThread;
  ServerStart serverStart;
  private HashMap<Connection, ClientThread> clientThreads = new HashMap<Connection, ClientThread>();

  public ServerListener(GameThread gameThread, ServerStart serverStart) {
    this.gameThread = gameThread;
    this.serverStart = serverStart;
  }

  @Override
  public void connected(Connection connection) {
    System.out.println("[SERVER] Client connected creating new thread.");
    //create new thread
    clientThreads.put(connection, new ClientThread(
        connection,
        gameThread,
        serverStart));
  }

  @Override
  public void disconnected(Connection connection) {
    //Stop the client thread.
    System.out.println("[SERVER] Client has disconnected, stopping the thread");
    ServerStart.PlayerConnection c = (ServerStart.PlayerConnection) connection;
    if (c.player != null) {
      gameThread.getPlayers().remove(c.player.pid);
      NetworkProtocol.RemoveObject removeCharacter = new NetworkProtocol.RemoveObject();
      removeCharacter.player = c.player;
      serverStart.server.sendToAllTCP(removeCharacter);
    }

    clientThreads.remove(connection);
    connection.close();
  }

}
