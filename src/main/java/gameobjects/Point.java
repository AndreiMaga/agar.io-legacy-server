package gameobjects;

import org.joml.Vector3f;

public class Point {
  volatile public Vector3f pos;
  volatile public int pid;
  public boolean toRemove = false;

  public Point(int pid, Vector3f pos) {
    this.pid = pid;
    this.pos = pos;
  }
}
