package gameobjects;

import org.joml.Vector3f;

import java.awt.*;

public class Player {
  volatile public String name;
  volatile public Vector3f posOnGrid;
  volatile public Color color;
  volatile public int pid;
  volatile public Vector3f posOnScreen;
  volatile public int radius;
  public boolean isDead = false;

  public Player(String name, Vector3f posOnGrid, int pid, int radius, Color color) {
    this.name = name;
    this.posOnGrid = posOnGrid;
    this.pid = pid;
    this.radius = radius;
    this.color = color;
  }

}
